#!/bin/bash

[[ $# < 1 ]] && echo "Usage: $0 files" && exit 1

FILES="$@"

function space_r_space()
{
	PATTERN=$1

	while grep -q "[\*_a-zA-Z0-9]${PATTERN}[\*_a-zA-Z0-9]" $FILES; do
		sed "s/\([\*_a-zA-Z0-9]\)${PATTERN}\([\*_a-zA-Z0-9]\)/\1 ${PATTERN} \2/g" -i $FILES
	done
}

function r_space()
{
	PATTERN=$1

	while grep -q "[\*_a-zA-Z0-9]${PATTERN}[\*_a-zA-Z0-9]" $FILES; do
		sed "s/\([\*_a-zA-Z0-9]\)${PATTERN}\([\*_a-zA-Z0-9]\)/\1${PATTERN} \2/g" -i $FILES
	done

}

space_r_space '\*='
space_r_space '='
space_r_space '=='
space_r_space '<'
space_r_space '>'
space_r_space '<='
space_r_space '>='

r_space ','
r_space ';'

