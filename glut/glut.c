#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>


#include <GL/glx.h>
#include <GL/gl.h>

#include "glut.h"


static Display * dpy;
static Window win;

static glut_display_func display_func = NULL;
static glut_keyboard_func keyboard_func = NULL;
static glut_reshape_func reshape_func = NULL;
static glut_idle_func idle_func = NULL;


static struct glut_window main_window = {0,};


#define DEFAULT_WIN_WIDTH	400
#define DEFAULT_WIN_HEIGHT	300




void glutSwapBuffers(void)
{
	glXSwapBuffers(dpy, win);
}

void glutInit(int *argc, char **argv)
{
	(void)argc; (void)argv;

	memset(&main_window, 0, sizeof(main_window));
}

void glutInitWindowSize(int width, int height)
{
	main_window.width  = width;
	main_window.height = height;
}

void glutInitWindowPosition(int x, int y)
{
	main_window.x = x;
	main_window.y = y;
}

static int attributeList[] = { GLX_RGBA, GLX_DOUBLEBUFFER, None };

static Bool WaitForNotify(Display *d, XEvent *e, char *arg) 
{
	return (e->type == MapNotify) && (e->xmap.window == (Window)arg); 
}

int glutCreateWindow(char *name)
{
	XVisualInfo *vi;
	Colormap cmap;
	XSetWindowAttributes swa;
	XSizeHints hint;
	GLXContext cx;
	XEvent event;
	int width, height;


	/* get a connection */
	dpy = XOpenDisplay(NULL);
	if (dpy == NULL) {
		fprintf(stderr,"Could not open X display\n");
		exit(1);
	}

	/* get an appropriate visual */
	vi = glXChooseVisual(dpy, DefaultScreen(dpy), attributeList);
	if (vi == NULL) {
		fprintf(stderr, "No suitable visual for glx\n");
		exit(1);
	}

	/* create a GLX context */
	cx = glXCreateContext(dpy, vi, 0, GL_TRUE);

	/* create a color map */
	cmap = XCreateColormap(dpy, RootWindow(dpy, vi->screen),
			vi->visual, AllocNone);

	/* create a window */
	width = main_window.width ? : DEFAULT_WIN_WIDTH;
	height = main_window.height ? : DEFAULT_WIN_HEIGHT;
	main_window.width = width;
	main_window.height = height;

	hint.x = main_window.x;
	hint.y = main_window.y;
	hint.width = width;
	hint.height = height;
	hint.flags = PPosition | PSize;
	swa.colormap = cmap;
	swa.border_pixel = 0;
	swa.event_mask = StructureNotifyMask;
	win = XCreateWindow(dpy, RootWindow(dpy, vi->screen), 0, 0, width, height,
			0, vi->depth, InputOutput, vi->visual,
			CWBorderPixel|CWColormap|CWEventMask, &swa);
	XSetStandardProperties (dpy, win, name, name, None, NULL, 0, &hint);

	XMapWindow(dpy, win);
	XIfEvent(dpy, &event, WaitForNotify, (char*)win);
	XSelectInput(dpy, win, KeyPressMask | StructureNotifyMask | ExposureMask);

	/* connect the context to the window */
	glXMakeCurrent(dpy, win, cx);


	return 1; /* FIXME */
}

void glutDisplayFunc(void (*func)(void))
{
	display_func = func;
}

void glutKeyboardFunc(void (*func)(unsigned char key,
                                   int x, int y))
{
	keyboard_func = func;
}


void glutReshapeFunc(void (*func)(int width, int height))
{
	reshape_func = func;
}

void glutIdleFunc(void (*func)(void))
{
	idle_func = func;
}

void glutMainLoop(void)
{
	int k;
	char buf[80];
	XEvent xev;
	KeySym keysym;
	XComposeStatus status;

	if (!reshape_func) {
		fprintf(stderr, "No reshape_func set!\n");
		exit(1);
	}
	reshape_func(main_window.width, main_window.height);

	if (display_func)
		display_func();

	while (1) {
		if (XPending(dpy) > 0) {
			XNextEvent(dpy,&xev);
			switch(xev.type) {
			case KeyPress:
				XLookupString((XKeyEvent *)&xev,buf,80,&keysym,&status);
				switch(keysym) {
				case XK_Up:
					k = KEY_UP;
					break;
				case XK_Down:
					k = KEY_DOWN;
					break;
				case XK_Left:
					k = KEY_LEFT;
					break;
				case XK_Right:
					k = KEY_RIGHT;
					break;
				case XK_Escape:
					k = KEY_ESCAPE;
					break;
				default:
					k = keysym;
				}

				keyboard_func(k, 0, 0);
				break;
			case ConfigureNotify:
				{
					int width,height;
					width = xev.xconfigure.width;
					height = xev.xconfigure.height;
					glXWaitX();
					reshape_func(width, height);
				}
				break;
			}
		} else {
			if (idle_func)
				idle_func();
		}
	}
}
