#ifndef GLUT_H
#define GLUT_H

typedef void (*glut_reshape_func) (int, int);
typedef void (*glut_keyboard_func) (unsigned char, int, int);
typedef void (*glut_display_func) (void);
typedef void (*glut_idle_func) (void);

extern void glutSwapBuffers(void);
extern void glutInit(int *argc, char **argv);
extern void glutDisplayFunc(void (*func)(void));
extern void glutKeyboardFunc(void (*func)(unsigned char key, int x, int y));
extern void glutReshapeFunc(void (*func)(int width, int height));
extern void glutInitWindowSize(int width, int height);
extern void glutInitWindowPosition(int x, int y);

extern int glutCreateWindow(char *);


struct glut_window {
	int width;
	int height;
	int x;
	int y;

	char *name;
};

#define KEY_UP     0xe000
#define KEY_DOWN   0xe001
#define KEY_LEFT   0xe002
#define KEY_RIGHT  0xe003
#define KEY_ESCAPE 0xe004

#endif /* glut.h */
